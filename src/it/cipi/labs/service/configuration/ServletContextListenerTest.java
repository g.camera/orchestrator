package it.cipi.labs.service.configuration;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import it.cipi.labs.service.utilities.ClientREST;
import it.cipi.labs.service.utilities.LoggerUtilities;
import it.cipi.labs.service.utilities.PropertiesUtilities;
import it.cipi.labs.service.utilities.SSSUtilities;

@WebListener
public class ServletContextListenerTest implements ServletContextListener{

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		ServletContext sc = arg0.getServletContext();
		Properties myServletProperties= (Properties)sc.getAttribute("Properties");
	
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext sc= arg0.getServletContext();
		System.out.println("----->Server is starting now!!!");
		LoggerUtilities.initLogger();
		String warName = sc.getContextPath().length() == 0 ? "ROOT" : sc.getContextPath().substring(1);
		String propertiesFileName;
		try {
			propertiesFileName = System.getProperty("catalina.home") + "/conf/" + warName + ".conf";
			Properties myServletProperties= PropertiesUtilities.readProperties(propertiesFileName);
			
			SSSUtilities sssUtilities = new SSSUtilities(myServletProperties.getProperty("server"), myServletProperties.getProperty("username"), myServletProperties.getProperty("password"));
			sssUtilities.initSSSUtilities();
			
			sc.setAttribute("Properties", myServletProperties);
			sc.setAttribute("SSSUtilities", sssUtilities);
			
			
		}catch(Exception e) {
			Logger.getRootLogger().error("Cannot read properties. File not found:"+e);
		}
		
		
		
	}

}
