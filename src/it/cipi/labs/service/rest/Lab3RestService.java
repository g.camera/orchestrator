package it.cipi.labs.service.rest;


import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

@Path("/testclass")
public class Lab3RestService {
	@Context private ServletContext sc;

	@GET
	@Path("simpleget")
	@Produces(MediaType.APPLICATION_JSON)
	public String simpleGet() {
		Gson gson = new Gson ();
		HashMap<String, String> simpleGetResponse= new HashMap<String,String>();
		simpleGetResponse.put("greetings", "hello world");
		String response= gson.toJson(simpleGetResponse);
		return response;

	}

	@GET
	@Path("getwithqueryparameter")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWithQuery(@QueryParam("color") String color) {
		Gson gson = new Gson ();
		HashMap<String, String> getwithqueryparameterResponse= new HashMap<String,String>();
		getwithqueryparameterResponse.put("greetings", "hello world");
		getwithqueryparameterResponse.put("color", color);
		String response= gson.toJson(getwithqueryparameterResponse);
		return response;
	}

	@GET
	@Path("getwithpathparameter/{shape}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getWithPath(@PathParam("shape") String shape) {
		Gson gson = new Gson ();
		HashMap<String, String> getwithpathparameterResponse= new HashMap<String,String>();
		getwithpathparameterResponse.put("greetings", "hello world");
		getwithpathparameterResponse.put("shape", shape);
		String response= gson.toJson(getwithpathparameterResponse);
		return response;
	}

	@POST
	@Path("newdata")
	public String newData (String realObjectNewData) {
		Gson gson = new Gson ();
		
		return realObjectNewData;
	}
	
	

}
