package it.cipi.labs.service.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtilities {

	public static Properties readProperties(String filepath) throws Exception {
		Properties properties = new Properties();
		try {
			InputStream is = new FileInputStream(new File(filepath));
			properties.load(is);
			is.close();
		} catch (IOException e) {
			throw new Exception (e);
		}
		return properties;

	}
}
