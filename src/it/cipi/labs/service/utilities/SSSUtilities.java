package it.cipi.labs.service.utilities;

import java.util.Set;

import org.apache.log4j.Logger;

import net.spreadsheetspace.sdk.Sdk;
import net.spreadsheetspace.sdk.StatusCode;
import net.spreadsheetspace.sdk.model.GenerateKeyDescriptor;
import net.spreadsheetspace.sdk.model.ViewDescriptor;

public class SSSUtilities {
	private Sdk sdk;
	private String server;
	private String username;
	private String password;
	private boolean initialized;
	
	public SSSUtilities (String server, String username, String password) {
		this.server=server;
		this.username=username;
		this.password=password;
		this.initialized=false;
	}
	
	public void initSSSUtilities () {
		try {
			this.sdk = new Sdk(this.server, this.username, this.password);
			Logger.getRootLogger().info("Generating keys....");
			GenerateKeyDescriptor generateKeyDescriptor = sdk.generateKey();
			String privateKey = generateKeyDescriptor.getPrivateKey();
			Logger.getRootLogger().info(privateKey);
			Logger.getRootLogger().info("Chiave generata e salvata");
			this.initialized=true;
		} catch (Exception e) {
			Logger.getRootLogger().error("Error in initialization ",e);
		}
	}

	public void createPrivateView(String tableName, String [][] table, Set<String> recipients) {
		if(this.initialized) {
			try {		
							
				Logger.getRootLogger().info("Creazione vista privata....");
				ViewDescriptor viewDescriptor = sdk.createPrivateView(tableName, recipients, table, null, false, false, table.length, table[0].length);
				
				if(viewDescriptor.getStatusCode() == StatusCode.OK) {
					Logger.getRootLogger().info("Vista privata creata");
				}else {
					Logger.getRootLogger().error("Error in view creation ");
				}
			} catch (Exception e) {
				Logger.getRootLogger().error("Error in view creation ",e);
			}
		}
		
	}
}
